import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Points_History.html',
    selector: 'page-points_-history',
    styleUrls: ['Points_History.scss']
})
export class Points_History {
    public totalPoints: string = "0";
    public type: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ionViewWillEnter() {
        this.pageIonViewWillEnter__j_184();
    }
    async pageIonViewWillEnter__j_184(event ? , currentItem ? ) {
        /* Run TypeScript */
        var customerCode = await this.Apperyio.data.getStorage("customer_code");
        var thisObject = this
        this.Apperyio.getService("get_balance_service").then(
            service => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                service.execute({
                    data: {},
                    params: {
                        "customer_code": customerCode
                    },
                    headers: {}
                }).subscribe(
                    (res: any) => {
                        if (res.hasOwnProperty('Customer')) {
                            thisObject.totalPoints = res.Customer.balance;
                        } else {
                            thisObject.totalPoints = '0';
                        }
                    },
                    (err: any) => {
                        console.log(err)
                    }
                )
            }
        )
    }
    private $aio_dataServices = {
        "get_total_points": "invokeService_get_total_points"
    }
    invokeService_get_total_points(cb ? : Function) {
        this.Apperyio.getService("get_balance_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['customer_code'], await this.$aio_mappingHelper.getStorageValue("customer_code", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.totalPoints = this.$aio_mappingHelper.updateData(this.totalPoints, [], this.$aio_mappingHelper.getSubdata(res, ["Customer", "balance"]));
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}