import {
    NgModule
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    FormsModule
} from '@angular/forms';
import {
    RouterModule
} from '@angular/router';
import {
    HttpClientModule
} from '@angular/common/http';
import {
    IonicModule
} from '@ionic/angular';
import {
    IonicStorageModule
} from '@ionic/storage';
import {
    ApperyioModule
} from "./scripts/apperyio/apperyio.module";
import {
    PipesModule
} from './scripts/pipes.module';
import {
    DirectivesModule
} from './scripts/directives.module';
import {
    ComponentsModule
} from './scripts/components.module';
import {
    CustomModulesModule
} from './scripts/custom-modules.module';
import {
    app
} from './app';
import {
    AppRoutingModule
} from './app-routing.module';
import {
    ExportedClass as login_api_service
} from './scripts/services/login_api_service';
import {
    ExportedClass as get_users_detail
} from './scripts/services/get_users_detail';
import {
    ExportedClass as update_user_service
} from './scripts/services/update_user_service';
import {
    ExportedClass as get_balance_service
} from './scripts/services/get_balance_service';
import {
    ExportedClass as register_service_new
} from './scripts/services/register_service_new';
import {
    ExportedClass as register_service_already
} from './scripts/services/register_service_already';
import {
    ExportedClass as forgot_password_service
} from './scripts/services/forgot_password_service';
import {
    ExportedClass as get_store_regions
} from './scripts/services/get_store_regions';
import {
    ExportedClass as get_all_store_service
} from './scripts/services/get_all_store_service';
import {
    ExportedClass as get_store_details_service
} from './scripts/services/get_store_details_service';
import {
    ExportedClass as change_password_service
} from './scripts/services/change_password_service';
import {
    ExportedClass as AuthInterceptor
} from './scripts/custom/AuthInterceptor';
import {
    ExportedClass as CountriesPhoneCode
} from './scripts/custom/CountriesPhoneCode';
import {
    WebView
} from '@ionic-native/ionic-webview/ngx';
import {
    Device
} from '@ionic-native/device/ngx';
import {
    SplashScreen
} from '@ionic-native/splash-screen/ngx';
import {
    StatusBar
} from '@ionic-native/status-bar/ngx';
import {
    Keyboard
} from '@ionic-native/keyboard/ngx';
@NgModule({
    declarations: [
        app
    ],
    imports: [
        BrowserModule,
        FormsModule,
        IonicModule.forRoot(),
        HttpClientModule,
        ApperyioModule,
        PipesModule,
        DirectivesModule,
        ComponentsModule,
        CustomModulesModule,
        IonicStorageModule.forRoot(),
        AppRoutingModule
    ],
    bootstrap: [
        app
    ],
    entryComponents: [
        //app
    ],
    providers: [
        StatusBar,
        SplashScreen,
        WebView,
        Device,
        Keyboard,
        login_api_service,
        get_users_detail,
        update_user_service,
        get_balance_service,
        register_service_new,
        register_service_already,
        forgot_password_service,
        get_store_regions,
        get_all_store_service,
        get_store_details_service,
        change_password_service,
        AuthInterceptor,
        CountriesPhoneCode,
    ]
})
export class AppModule {}