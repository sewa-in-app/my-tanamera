import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    DomSanitizer
} from '@angular/platform-browser';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Store_Detail.html',
    selector: 'page-store_-detail',
    styleUrls: ['Store_Detail.scss']
})
export class Store_Detail {
    public id: number;
    public storeName: string;
    public quinosName: string;
    public address: string;
    public operationHours: string;
    public phoneNumber: string;
    public lat: string;
    public lng: string;
    public token: string;
    public store_id: string;
    public region: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    getMapUrl() {
        var lat = this.lat;
        var lng = this.lng
        var address = this.address;
        if (this.lat == null || this.lat == undefined) {
            if (this.lat == null || this.lat == undefined) {
                return this.domSanitizer.bypassSecurityTrustResourceUrl(`https://www.google.com/maps/embed/v1/place?q=${encodeURIComponent(address)}&key=AIzaSyBZOcpmtsRWCHDxt4__O8SotJyCwdPs3Zs`);
            }
        }
        return this.domSanitizer.bypassSecurityTrustResourceUrl(`https://www.google.com/maps/embed/v1/place?q=${lat}, ${lng}&key=AIzaSyBZOcpmtsRWCHDxt4__O8SotJyCwdPs3Zs`);
        // token peter
        // AIzaSyB3iiSd3mNstfQ5G9TGzd9ik0bbU07spUI
        // token yoga
        // AIzaSyD2ueQW7wFbX6lOQPUq4CZpwT5bTtzIL94
    }
    constructor(private domSanitizer: DomSanitizer, public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {
    }
    ionViewWillEnter() {
        this.pageIonViewWillEnter__j_93();
    }
    async pageIonViewWillEnter__j_93(event ? , currentItem ? ) {
        /* Set variable */
        this.id = this.Apperyio.getRouteParam("id")
        /* Invoke data service */
        this.invokeService_get_store_detail();
    }
    async backbtncomponentClick__j_96(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Stores', this.region);
    }
    private $aio_dataServices = {
        "get_store_detail": "invokeService_get_store_detail"
    }
    invokeService_get_store_detail(cb ? : Function) {
        this.Apperyio.getService("get_store_details_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                params = this.$aio_mappingHelper.updateData(params, ['id'], this.$aio_mappingHelper.getSubdata(this.id, []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.token = this.$aio_mappingHelper.updateData(this.token, [], await this.$aio_mappingHelper.getStorageValue("api_token_only", []));
                        this.storeName = this.$aio_mappingHelper.updateData(this.storeName, [], this.$aio_mappingHelper.getSubdata(res, ["store", "store_label"]));
                        this.address = this.$aio_mappingHelper.updateData(this.address, [], this.$aio_mappingHelper.getSubdata(res, ["store", "address"]));
                        this.operationHours = this.$aio_mappingHelper.updateData(this.operationHours, [], this.$aio_mappingHelper.getSubdata(res, ["store", "operation_hours"]));
                        this.phoneNumber = this.$aio_mappingHelper.updateData(this.phoneNumber, [], this.$aio_mappingHelper.getSubdata(res, ["store", "phone_number"]));
                        this.lat = this.$aio_mappingHelper.updateData(this.lat, [], this.$aio_mappingHelper.getSubdata(res, ["store", "lat"]));
                        this.lng = this.$aio_mappingHelper.updateData(this.lng, [], this.$aio_mappingHelper.getSubdata(res, ["store", "lng"]));
                        this.store_id = this.$aio_mappingHelper.updateData(this.store_id, [], this.$aio_mappingHelper.getSubdata(res, ["store", "logix1_store_id"]));
                        this.id = this.$aio_mappingHelper.updateData(this.id, [], this.$aio_mappingHelper.getSubdata(res, ["store", "id"]));
                        this.region = this.$aio_mappingHelper.updateData(this.region, [], this.$aio_mappingHelper.getSubdata(res, ["store", "region"]));
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}