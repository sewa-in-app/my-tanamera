import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    ExportedClass as CountriesPhoneCode
} from '../scripts/custom/CountriesPhoneCode';
import {
    $aio_empty_object
} from '../scripts/interfaces';
@Component({
    templateUrl: 'Register.html',
    selector: 'page-register',
    styleUrls: ['Register.scss']
})
export class Register {
    public customer_code: string;
    public first_name: string;
    public sur_name: string;
    public email: string;
    public phone_number: number;
    public password: string;
    public country_code: string;
    public selectedCountry: string;
    public currentItem: any = null;
    public mappingData: any = {};
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef, public CountriesPhoneCode: CountriesPhoneCode) {}
    ionViewWillEnter() {
        this.pageIonViewWillEnter__j_198();
    }
    async pageIonViewWillEnter__j_198(event ? , currentItem ? ) {
        /* Run TypeScript */
        this.country_code = "+62";
    }
    async select1IonChange__j_206(event ? , currentItem ? ) {
        /* Run TypeScript */
        var country = this.selectedCountry;
        this.country_code = country;
    }
    async input8Keyup__j_213(event ? , currentItem ? ) {
        /* Run TypeScript */
        this.phone_number = Number(this.phone_number);
        if (this.phone_number == 0) {
            this.phone_number = null
        }
        console.log(this.phone_number)
    }
    async btnsignupClick__j_214(event ? , currentItem ? ) {
        /* Run TypeScript */
        var firstName = this.first_name;
        var surName = this.sur_name;
        var email = this.email;
        var phoneNumber = this.country_code + this.phone_number;
        var password = this.password;
        var customerCode = phoneNumber;
        this.Apperyio.getController("LoadingController").create({
            message: 'Please wait...',
            spinner: 'crescent',
            duration: 2000
        }).then(loading => loading.present());
        this.Apperyio.getService("register_service_new").then(
            service => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                service.execute({
                    data: {},
                    params: {
                        "first_name": firstName,
                        "sur_name": surName,
                        "email": email,
                        "password": password,
                        "phone_number": phoneNumber,
                        "customer_code": customerCode
                    },
                    headers: {}
                }).subscribe(
                    (res: any) => {
                        this.Apperyio.getController("ToastController").create({
                            header: 'Response',
                            message: res.Message,
                            position: 'top',
                            buttons: [{
                                side: 'start',
                                icon: 'checkmark',
                                handler: () => {
                                    console.log('Favorite clicked');
                                }
                            }, {
                                text: 'Done',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]
                        }).then(toast => toast.present());
                        this.Apperyio.navigateTo("Login");
                    },
                    (err: any) => {
                        this.Apperyio.navigateTo("Register");
                        console.log(err)
                    }
                )
            }
        );
        this.first_name = "";
        this.sur_name = "";
        this.email = "";
        // this.phone_number = "";
        this.password = "";
    }
}