import {
    NgModule
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    FormsModule
} from '@angular/forms';
import {
    RouterModule
} from '@angular/router';
import {
    Routes
} from '@angular/router';
import {
    IonicModule
} from '@ionic/angular';
import {
    NiceTabs
} from './NiceTabs';
import {
    PipesModule
} from '../scripts/pipes.module';
import {
    DirectivesModule
} from '../scripts/directives.module';
import {
    ComponentsModule
} from '../scripts/components.module';
import {
    CustomModulesModule
} from '../scripts/custom-modules.module';
const routes: Routes = [{
    path: '',
    component: NiceTabs,
    children: [{
            path: 'Tabs',
            children: [{
                path: '',
                loadChildren: '../Tabs/Tabs.module#TabsPageModule'
            }]
        },
        {
            path: 'Profile_Tabs_Plugin',
            children: [{
                path: '',
                loadChildren: '../Profile_Tabs_Plugin/Profile_Tabs_Plugin.module#Profile_Tabs_PluginPageModule'
            }]
        },
        {
            path: 'Info',
            children: [{
                path: '',
                loadChildren: '../Info/Info.module#InfoPageModule'
            }]
        },
        {
            path: '',
            redirectTo: 'Tabs',
            pathMatch: 'full'
        }
    ]
}];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class PageRoutingModule {}
@NgModule({
    declarations: [
        NiceTabs
    ],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PipesModule,
        DirectivesModule,
        ComponentsModule,
        CustomModulesModule, PageRoutingModule
    ],
    exports: [
        NiceTabs
    ]
})
export class NiceTabsPageModule {}