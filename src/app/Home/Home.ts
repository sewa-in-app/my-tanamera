import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Home.html',
    selector: 'page-home',
    styleUrls: ['Home.scss']
})
export class Home {
    public slideOpts: any = {
        centeredSlides: true,
        initialSlide: 0,
        speed: 200,
        autoplay: true,
        spaceBetween: 0,
        loop: true,
        loopAdditionalSlides: 2
    };;
    public balance: string = "0";
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ionViewWillEnter() {
        this.pageIonViewWillEnter__j_66();
    }
    async pageIonViewWillEnter__j_66(event ? , currentItem ? ) {
        let mappingData: any = {};
        /* Run TypeScript */
        var customerCode = await this.Apperyio.data.getStorage("customer_code");
        var thisObject = this
        this.Apperyio.getService("get_balance_service").then(
            service => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                service.execute({
                    data: {},
                    params: {
                        "customer_code": customerCode
                    },
                    headers: {}
                }).subscribe(
                    (res: any) => {
                        if (res.hasOwnProperty('Customer')) {
                            thisObject.balance = res.Customer.balance;
                        } else {
                            thisObject.balance = '0';
                        }
                    },
                    (err: any) => {
                        console.log(err)
                    }
                )
            }
        )
        /* Mapping */
        mappingData.j_73__text = await this.$aio_mappingHelper.getStorageValue("name", []);
        /* Run TypeScript */
        // let is_login = await this.Apperyio.data.getStorage("user_id");
        // if(is_login){
        //     this.Apperyio.navigateTo("Tabs"/*, optional, params, here */);
        // } else {
        //     this.Apperyio.navigateTo("Login"/*, optional, params, here */);
        // }
        this.mappingData = { ...this.mappingData,
            ...mappingData
        };
    }
    private $aio_dataServices = {
        "get_username": "invokeService_get_username",
        "get_balance": "invokeService_get_balance"
    }
    invokeService_get_username(cb ? : Function) {
        this.Apperyio.getService("get_users_detail").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        mappingData.j_73__text = await this.$aio_mappingHelper.getStorageValue("name", []);
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
    invokeService_get_balance(cb ? : Function) {
        this.Apperyio.getService("get_balance_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['customer_code'], await this.$aio_mappingHelper.getStorageValue("customer_code", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.balance = this.$aio_mappingHelper.updateData(this.balance, [], this.$aio_mappingHelper.getSubdata(res, ["Customer", "balance"]));
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}