import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    HttpClient
} from '@angular/common/http';
import {
    $aio_empty_object
} from '../scripts/interfaces';
@Component({
    templateUrl: 'Login.html',
    selector: 'page-login',
    styleUrls: ['Login.scss']
})
export class Login {
    public email: string;
    public password: string;
    public currentItem: any = null;
    public mappingData: any = {};
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    async html4Click__j_11(event ? , currentItem ? ) {
        /* Run TypeScript */
        var email = this.email;
        var password = this.password;
        this.Apperyio.getController("LoadingController").create({
            message: 'Please wait...',
            spinner: 'crescent',
            duration: 1000
        }).then(loading => loading.present());
        this.Apperyio.getService("login_api_service").then(
            service => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                service.execute({
                    data: {},
                    params: {
                        'email': email,
                        'password': password
                    },
                    headers: {}
                }).subscribe(
                    (res: any) => {
                        if (res.status == 'Success') {
                            this.Apperyio.data.setStorage("api_token", res.api_token);
                            this.Apperyio.data.setStorage("user_id", res.user_id);
                            this.Apperyio.data.setStorage("name", res.first_name.toUpperCase());
                            this.Apperyio.data.setStorage("profile_name", res.first_name.toUpperCase() + "," + " YOU HAVE");
                            this.Apperyio.data.setStorage("email", res.email);
                            this.Apperyio.data.setStorage("customer_code", res.customer_code);
                            this.Apperyio.data.setStorage("phone_number", res.phone_number);
                            this.Apperyio.data.setStorage("date_of_birth", res.date_of_birth);
                            this.Apperyio.data.setStorage("api_token_only", res.api_token);
                            this.Apperyio.data.setStorage("header_api_token", 'Bearer ' + res.api_token);
                            this.Apperyio.getController("ToastController").create({
                                header: 'Response',
                                message: 'Succesfully Login',
                                position: 'top',
                                buttons: [{
                                    side: 'start',
                                    icon: 'checkmark',
                                    handler: () => {
                                        console.log('Favorite clicked');
                                    }
                                }, {
                                    text: 'Done',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }]
                            }).then(toast => toast.present());
                            this.Apperyio.navigateTo("Tabs");
                        } else {
                            this.Apperyio.getController("ToastController").create({
                                header: 'Response',
                                message: 'Incorrect email or password',
                                position: 'top',
                                buttons: [{
                                    side: 'start',
                                    icon: 'warning',
                                    handler: () => {
                                        console.log('Favorite clicked');
                                    }
                                }, {
                                    text: 'Done',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }]
                            }).then(toast => toast.present());
                            this.Apperyio.navigateTo("Login");
                        }
                    },
                    (err: any) => {
                        console.log(err)
                    }
                )
            }
        );
        // this.Apperyio.getService("login_api_service").then(
        //     service => {
        //         if (!service) {
        //             console.log("Error. Service was not found.");
        //             return;
        //         }
        //         service.execute({
        //             data: {},
        //             params: {'email':email,'password':password},
        //             headers: {}
        //         }).subscribe(
        //             (res: any) => {
        //                 if(res.status=='Success'){
        //                     this.Apperyio.getController("ToastController").create({
        //                         header: 'Response',
        //                         message: 'Successfully login',
        //                         duration: 1000,
        //                         position: 'top',
        //                     }).then(toast => toast.present());
        //                     let controller = this.Apperyio.getController("MenuController"); 
        //                     // controller.enable(true,'custom');
        //                     this.Apperyio.navigateTo("Tabs");
        //                 } else{
        //                     this.Apperyio.getController("ToastController").create({
        //                         header: 'Response',
        //                         message: 'Failed to login incorrect email or password',
        //                         position: 'top',
        //                         duration: 2000
        //                     }).then(toast => toast.present());
        //                 }
        //                 this.Apperyio.getController("LoadingController").dismiss();
        //             },
        //             (err: any) => {
        //                 console.log(err);
        //                 this.Apperyio.getController("LoadingController").dismiss();
        //             }
        //         )
        //     }
        // )
    }
    async html5Click__j_13(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Register');
    }
    async html6Click__j_15(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Forgot_Password');
    }
}