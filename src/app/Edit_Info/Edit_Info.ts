import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Edit_Info.html',
    selector: 'page-edit_-info',
    styleUrls: ['Edit_Info.scss']
})
export class Edit_Info {
    public message: string;
    public dateOfBirth: string;
    public first_name: string;
    public email: string;
    public phone_number: string;
    public sur_name: string;
    public date: string;
    public month: string;
    public year: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ionViewDidEnter() {
        this.pageIonViewDidEnter__j_238();
    }
    async pageIonViewDidEnter__j_238(event ? , currentItem ? ) {
        /* Invoke data service */
        this.invokeService_get_user_details();
    }
    async image1Click__j_242(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Settings_clone_1');
    }
    async html2Click__j_256(event ? , currentItem ? ) {
        /* Invoke data service */
        this.invokeService_update_user_details();
    }
    private $aio_dataServices = {
        "get_user_details": "invokeService_get_user_details",
        "update_user_details": "invokeService_update_user_details"
    }
    invokeService_get_user_details(cb ? : Function) {
        this.Apperyio.getService("get_users_detail").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.phone_number = this.$aio_mappingHelper.updateData(this.phone_number, [], this.$aio_mappingHelper.getSubdata(res, ["phone_number"]));
                        this.first_name = this.$aio_mappingHelper.updateData(this.first_name, [], this.$aio_mappingHelper.getSubdata(res, ["first_name"]));
                        this.sur_name = this.$aio_mappingHelper.updateData(this.sur_name, [], this.$aio_mappingHelper.getSubdata(res, ["sur_name"]));
                        this.email = this.$aio_mappingHelper.updateData(this.email, [], this.$aio_mappingHelper.getSubdata(res, ["email"]));
                        this.dateOfBirth = this.$aio_mappingHelper.updateData(this.dateOfBirth, [], this.$aio_mappingHelper.getSubdata(res, ["date_of_birth"]));
                        this.date = this.$aio_mappingHelper.updateData(this.date, [], ((value) => {
                            return value.split("D")[0];
                        })(this.$aio_mappingHelper.getSubdata(res, ["date_of_birth"])));
                        this.month = this.$aio_mappingHelper.updateData(this.month, [], ((value) => {
                            return value.split("M")[0];
                        })(this.$aio_mappingHelper.getSubdata(res, ["date_of_birth"])));
                        this.year = this.$aio_mappingHelper.updateData(this.year, [], ((value) => {
                            return value.split("Y")[0];
                        })(this.$aio_mappingHelper.getSubdata(res, ["date_of_birth"])));
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
    invokeService_update_user_details(cb ? : Function) {
        this.Apperyio.getService("update_user_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Run TypeScript */
                this.dateOfBirth = this.year + "-" + this.month + "-" + this.date;
                console.log(this.dateOfBirth)
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['date_of_birth'], ((value) => {
                    return value.split("T")[0];
                })(this.$aio_mappingHelper.getSubdata(this.dateOfBirth, [])));
                params = this.$aio_mappingHelper.updateData(params, ['first_name'], this.$aio_mappingHelper.getSubdata(this.first_name, []));
                params = this.$aio_mappingHelper.updateData(params, ['email'], this.$aio_mappingHelper.getSubdata(this.email, []));
                params = this.$aio_mappingHelper.updateData(params, ['sur_name'], this.$aio_mappingHelper.getSubdata(this.sur_name, []));
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                params = this.$aio_mappingHelper.updateData(params, ['phone_number'], this.$aio_mappingHelper.getSubdata(this.phone_number, []));
                /* Present Loading */
                await (async () => {
                    let options = {
                        'duration': 1,
                    }
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.message = this.$aio_mappingHelper.updateData(this.message, [], this.$aio_mappingHelper.getSubdata(res, ["Message"]));
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}