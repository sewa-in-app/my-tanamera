import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
@Component({
    templateUrl: 'SplashScreen.html',
    selector: 'page-splash-screen',
    styleUrls: ['SplashScreen.scss']
})
export class SplashScreen {
    public currentItem: any = null;
    public mappingData: any = {};
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ngOnInit() {
        this.pageNgOnInit__j_0();
    }
    async pageNgOnInit__j_0(event ? , currentItem ? ) {
        /* Run TypeScript */
        // let controller = this.Apperyio.getController("MenuController"); 
        let is_login = await this.Apperyio.data.getStorage("user_id");
        if (is_login) {
            this.Apperyio.navigateTo("Tabs"
                /*, optional, params, here */
            );
            // controller.enable(true,'custom');
        } else {
            this.Apperyio.navigateTo("Login"
                /*, optional, params, here */
            );
            // controller.enable(false,'custom');
        }
    }
}