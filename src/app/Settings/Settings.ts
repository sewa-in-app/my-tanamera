import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Settings.html',
    selector: 'page-settings',
    styleUrls: ['Settings.scss']
})
export class Settings {
    public firstName: string;
    public email: string;
    public phoneNumber: string;
    public surName: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ngOnInit() {
        this.pageNgOnInit__j_284();
    }
    async pageNgOnInit__j_284(event ? , currentItem ? ) {
        /* Invoke data service */
        this.invokeService_user_info_service();
    }
    async editinfoClick__j_291(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Edit_Info');
    }
    async notiflinkClick__j_302(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Notification_Settings');
    }
    async changepasslinkClick__j_309(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Change_Password');
    }
    async btnsignoutClick__j_328(event ? , currentItem ? ) {
        /* Run TypeScript */
        await this.Apperyio.data.setStorage("header_api_token", "");
        await this.Apperyio.data.setStorage("user_id", "");
        await this.Apperyio.data.setStorage("name", "");
        await this.Apperyio.data.setStorage("customer_code", "");
        await this.Apperyio.data.setStorage("profile_name", "");
        await this.Apperyio.data.setStorage("email", "");
        await this.Apperyio.data.setStorage("phone_number", "");
        await this.Apperyio.data.setStorage("api_token_only", "");
        this.Apperyio.navigateTo("Login"
            /*, optional, params, here */
        );
    }
    private $aio_dataServices = {
        "user_info_service": "invokeService_user_info_service"
    }
    invokeService_user_info_service(cb ? : Function) {
        this.Apperyio.getService("get_users_detail").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.firstName = this.$aio_mappingHelper.updateData(this.firstName, [], this.$aio_mappingHelper.getSubdata(res, ["first_name"]));
                        this.email = this.$aio_mappingHelper.updateData(this.email, [], await this.$aio_mappingHelper.getStorageValue("email", []));
                        this.phoneNumber = this.$aio_mappingHelper.updateData(this.phoneNumber, [], await this.$aio_mappingHelper.getStorageValue("phone_number", []));
                        this.surName = this.$aio_mappingHelper.updateData(this.surName, [], this.$aio_mappingHelper.getSubdata(res, ["sur_name"]));
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}