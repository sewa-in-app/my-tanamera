import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Profile.html',
    selector: 'page-profile',
    styleUrls: ['Profile.scss']
})
export class Profile {
    public Message: string;
    public current_balance: string = "0";
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ionViewWillEnter() {
        this.pageIonViewWillEnter__j_98();
    }
    async pageIonViewWillEnter__j_98(event ? , currentItem ? ) {
        let mappingData: any = {};
        /* Mapping */
        mappingData.j_106__text = await this.$aio_mappingHelper.getStorageValue("profile_name", []);
        /* Run TypeScript */
        var customerCode = await this.Apperyio.data.getStorage("customer_code");
        var thisObject = this
        this.Apperyio.getService("get_balance_service").then(
            service => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                service.execute({
                    data: {},
                    params: {
                        "customer_code": customerCode
                    },
                    headers: {}
                }).subscribe(
                    (res: any) => {
                        // console.log(res);
                        if (res.hasOwnProperty('Customer')) {
                            thisObject.current_balance = res.Customer.balance;
                        } else {
                            thisObject.current_balance = '0';
                        }
                    },
                    (err: any) => {
                        console.log(err)
                    }
                )
            }
        )
        this.mappingData = { ...this.mappingData,
            ...mappingData
        };
    }
    async text5Click__j_110(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Points_History');
    }
    async settingsmenuClick__j_114(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Settings');
    }
    private $aio_dataServices = {
        "get_userdata": "invokeService_get_userdata",
        "update_user": "invokeService_update_user",
        "get_current_balance": "invokeService_get_current_balance"
    }
    invokeService_get_userdata(cb ? : Function) {
        this.Apperyio.getService("get_users_detail").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        mappingData.j_106__text = await this.$aio_mappingHelper.getStorageValue("profile_name", []);
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
    invokeService_update_user(cb ? : Function) {
        this.Apperyio.getService("update_user_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
    invokeService_get_current_balance(cb ? : Function) {
        this.Apperyio.getService("get_balance_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}