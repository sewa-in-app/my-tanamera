import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Stores.html',
    selector: 'page-stores',
    styleUrls: ['Stores.scss']
})
export class Stores {
    public stores: any = [];
    public region: string;
    public token: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    loadStoresId(store) {
        this.Apperyio.navigateTo("Store_Detail", store.id);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ionViewWillEnter() {
        this.pageIonViewWillEnter__j_84();
    }
    async pageIonViewWillEnter__j_84(event ? , currentItem ? ) {
        /* Set variable */
        this.region = this.Apperyio.getRouteParam("region")
        /* Invoke data service */
        this.invokeService_get_stores_by_region();
    }
    async image1Click__j_89(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Outlets');
    }
    private $aio_dataServices = {
        "get_stores_by_region": "invokeService_get_stores_by_region",
        "service1": "invokeService_service1"
    }
    invokeService_get_stores_by_region(cb ? : Function) {
        this.Apperyio.getService("get_all_store_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['region'], this.$aio_mappingHelper.getSubdata(this.region, []));
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.token = this.$aio_mappingHelper.updateData(this.token, [], await this.$aio_mappingHelper.getStorageValue("api_token_only", []));
                        this.stores = this.$aio_mappingHelper.updateData(this.stores, [], this.$aio_mappingHelper.getSubdata(res, ["stores"]));
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
    invokeService_service1(cb ? : Function) {
        this.Apperyio.getService("get_store_details_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}