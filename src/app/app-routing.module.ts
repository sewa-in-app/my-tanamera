import {
    NgModule
} from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';
const routes: Routes = [{
        path: '',
        redirectTo: 'tabs',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: './Home/Home.module#HomePageModule',
    },
    {
        path: 'profile',
        loadChildren: './Profile/Profile.module#ProfilePageModule',
    },
    {
        path: 'splashscreen',
        loadChildren: './SplashScreen/SplashScreen.module#SplashScreenPageModule',
    },
    {
        path: 'login',
        loadChildren: './Login/Login.module#LoginPageModule',
    },
    {
        path: 'yourpoint',
        loadChildren: './YourPoint/YourPoint.module#YourPointPageModule',
    },
    {
        path: 'tabs',
        loadChildren: './Tabs/Tabs.module#TabsPageModule',
    },
    {
        path: 'profile',
        loadChildren: './Profile_Tabs_Plugin/Profile_Tabs_Plugin.module#Profile_Tabs_PluginPageModule',
    },
    {
        path: 'nicetabs',
        loadChildren: './NiceTabs/NiceTabs.module#NiceTabsPageModule',
    },
    {
        path: 'edit_info',
        loadChildren: './Edit_Info/Edit_Info.module#Edit_InfoPageModule',
    },
    {
        path: 'register',
        loadChildren: './Register/Register.module#RegisterPageModule',
    },
    {
        path: 'forgot_password',
        loadChildren: './Forgot_Password/Forgot_Password.module#Forgot_PasswordPageModule',
    },
    {
        path: 'outlets',
        loadChildren: './Outlets/Outlets.module#OutletsPageModule',
    },
    {
        path: 'stores/:region',
        loadChildren: './Stores/Stores.module#StoresPageModule',
    },
    {
        path: 'store_detail/:id',
        loadChildren: './Store_Detail/Store_Detail.module#Store_DetailPageModule',
    },
    {
        path: 'settings',
        loadChildren: './Settings/Settings.module#SettingsPageModule',
    },
    {
        path: 'change_password',
        loadChildren: './Change_Password/Change_Password.module#Change_PasswordPageModule',
    },
    {
        path: 'notification_settings',
        loadChildren: './Notification_Settings/Notification_Settings.module#Notification_SettingsPageModule',
    },
    {
        path: 'points_history',
        loadChildren: './Points_History/Points_History.module#Points_HistoryPageModule',
    },
    {
        path: 'settings_clone_1',
        loadChildren: './Settings_clone_1/Settings_clone_1.module#Settings_clone_1PageModule',
    },
    {
        path: 'profile_clone_1',
        loadChildren: './Profile_clone_1/Profile_clone_1.module#Profile_clone_1PageModule',
    },
];
@NgModule({
    imports: [RouterModule.forRoot(
        routes, {
            enableTracing: false,
            useHash: true
        }
    )],
    exports: [RouterModule]
})
export class AppRoutingModule {}