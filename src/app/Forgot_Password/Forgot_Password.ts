import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Forgot_Password.html',
    selector: 'page-forgot_-password',
    styleUrls: ['Forgot_Password.scss']
})
export class Forgot_Password {
    public email: string;
    public Message: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    @ViewChild('j_62', {
        static: false
    }) public j_62;
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    async image2Click__j_58(event ? , currentItem ? ) {
        /* Navigate to Page */
        this.Apperyio.navigateTo('Login');
    }
    async html2Click__j_63(event ? , currentItem ? ) {
        /* Invoke data service */
        this.invokeService_forgot_password();
    }
    private $aio_dataServices = {
        "forgot_password": "invokeService_forgot_password"
    }
    invokeService_forgot_password(cb ? : Function) {
        this.Apperyio.getService("forgot_password_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['email'], this.$aio_mappingHelper.getComponentPropValue.call(this, 'j_62', 'ionic4input', 'value'));
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.Message = this.$aio_mappingHelper.updateData(this.Message, [], this.$aio_mappingHelper.getSubdata(res, ["message"]));
                        /* Run TypeScript */
                        var message = this.Message;
                        this.Apperyio.getController("ToastController").create({
                            header: 'Response',
                            message: message,
                            position: 'top',
                            buttons: [{
                                side: 'start',
                                icon: 'checkmark',
                                handler: () => {
                                    console.log('Favorite clicked');
                                }
                            }, {
                                text: 'Done',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]
                        }).then(toast => toast.present());
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}