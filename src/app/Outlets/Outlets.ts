import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Outlets.html',
    selector: 'page-outlets',
    styleUrls: ['Outlets.scss']
})
export class Outlets {
    public regions: any = [];
    public token: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    loadStores(region) {
        this.Apperyio.navigateTo("Stores", region.region);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    ngOnInit() {
        this.pageNgOnInit__j_353();
    }
    async pageNgOnInit__j_353(event ? , currentItem ? ) {
        /* Invoke data service */
        this.invokeService_get_regions();
    }
    private $aio_dataServices = {
        "get_regions": "invokeService_get_regions"
    }
    invokeService_get_regions(cb ? : Function) {
        this.Apperyio.getService("get_store_regions").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        /* Mapping */
                        this.token = this.$aio_mappingHelper.updateData(this.token, [], await this.$aio_mappingHelper.getStorageValue("api_token_only", []));
                        this.regions = this.$aio_mappingHelper.updateData(this.regions, [], this.$aio_mappingHelper.getSubdata(res, ["regions"]));
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}