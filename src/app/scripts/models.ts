/**
 * Models generated from "Model and Storage" and models extracted from services.
 * To generate entity use syntax:
 * Apperyio.EntityAPI("<model_name>[.<model_field>]");
 */
export var models = {
    "String": {
        "type": "string"
    },
    "Number": {
        "type": "number"
    },
    "Any": {
        "type": "any"
    },
    "Function": {
        "type": "Function"
    },
    "Promise": {
        "type": "Promise"
    },
    "Boolean": {
        "type": "boolean"
    },
    "Observable": {
        "type": "Observable"
    },
    "update_user_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/13e9c2dc-1074-4078-a1f7-80c1597857d9/exec"
            },
            "method": {
                "type": "string",
                "default": "post"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "data"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "date_of_birth": {
                                "type": "string"
                            },
                            "header_api_token": {
                                "type": "string"
                            },
                            "first_name": {
                                "type": "string"
                            },
                            "sur_name": {
                                "type": "string"
                            },
                            "apiUrl": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "email": {
                                "type": "string"
                            },
                            "phone_number": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {
                            "Content-Type": {
                                "type": "string",
                                "default": "text/plain"
                            }
                        }
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "Message": {
                                        "type": "string",
                                        "default": "User Data Updated Succesfully"
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "get_balance_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/4006ce0d-f296-4129-9169-b2056c82d12a/exec"
            },
            "method": {
                "type": "string",
                "default": "get"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {}
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "api_url": {
                                "type": "string",
                                "default": "{Settings.quinos_api_url}"
                            },
                            "quinos_token": {
                                "type": "string",
                                "default": "{Settings.quinos_token}"
                            },
                            "customer_code": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "Customer": {
                                        "type": "object",
                                        "properties": {
                                            "city": {
                                                "type": "string"
                                            },
                                            "name": {
                                                "type": "string",
                                                "default": "George Purba"
                                            },
                                            "membership_id": {
                                                "type": "string",
                                                "default": "153"
                                            },
                                            "total_spent": {
                                                "type": "string",
                                                "default": "307000"
                                            },
                                            "notes": {
                                                "type": "string"
                                            },
                                            "joined": {
                                                "type": "null"
                                            },
                                            "occupation": {
                                                "type": "string"
                                            },
                                            "state": {
                                                "type": "string"
                                            },
                                            "total_visit": {
                                                "type": "string",
                                                "default": "4"
                                            },
                                            "mobile": {
                                                "type": "string"
                                            },
                                            "address": {
                                                "type": "string",
                                                "default": "south"
                                            },
                                            "balance": {
                                                "type": "string",
                                                "default": "27"
                                            },
                                            "postcode": {
                                                "type": "string"
                                            },
                                            "company_id": {
                                                "type": "string",
                                                "default": "197"
                                            },
                                            "expired": {
                                                "type": "null"
                                            },
                                            "phone": {
                                                "type": "string"
                                            },
                                            "dob": {
                                                "type": "string",
                                                "default": "1981-04-01"
                                            },
                                            "email": {
                                                "type": "string",
                                                "default": "george.purba@gmail.com"
                                            },
                                            "modified": {
                                                "type": "string",
                                                "default": "2020-03-19 16:19:50"
                                            },
                                            "oid": {
                                                "type": "string",
                                                "default": "5e7331352e5f5"
                                            },
                                            "created": {
                                                "type": "string",
                                                "default": "2020-03-19 15:45:41"
                                            },
                                            "gender": {
                                                "type": "string",
                                                "default": "M"
                                            },
                                            "id": {
                                                "type": "string",
                                                "default": "157563"
                                            },
                                            "code": {
                                                "type": "string",
                                                "default": "082110091009"
                                            },
                                            "active": {
                                                "type": "string",
                                                "default": "1"
                                            },
                                            "last_visit": {
                                                "type": "string",
                                                "default": "2020-03-19"
                                            }
                                        }
                                    },
                                    "Membership": {
                                        "type": "object",
                                        "properties": {
                                            "Discount4": {
                                                "type": "array",
                                                "items": [{
                                                    "type": "null"
                                                }]
                                            },
                                            "discount2_id": {
                                                "type": "null"
                                            },
                                            "Discount3": {
                                                "type": "array",
                                                "items": [{
                                                    "type": "null"
                                                }]
                                            },
                                            "name": {
                                                "type": "string",
                                                "default": "MyTanamera"
                                            },
                                            "discount4_id": {
                                                "type": "null"
                                            },
                                            "discount3_id": {
                                                "type": "null"
                                            },
                                            "Discount2": {
                                                "type": "array",
                                                "items": [{
                                                    "type": "null"
                                                }]
                                            },
                                            "Discount1": {
                                                "type": "array",
                                                "items": [{
                                                    "type": "null"
                                                }]
                                            },
                                            "discount1_id": {
                                                "type": "null"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "change_password_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/0e769426-6789-4d10-a1cf-025e7753eb46/exec"
            },
            "method": {
                "type": "string",
                "default": "post"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "data"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "new_password": {
                                "type": "string"
                            },
                            "header_api_token": {
                                "type": "string"
                            },
                            "current_password": {
                                "type": "string"
                            },
                            "apiUrl": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "confirm_password": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {
                            "Content-Type": {
                                "type": "string",
                                "default": "text/plain"
                            }
                        }
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "message": {
                                        "type": "string",
                                        "default": "Password succesfully updated"
                                    },
                                    "status": {
                                        "type": "number",
                                        "default": 200
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "login_api_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/ae47b231-0a46-44c3-be89-e018b31271f9/exec"
            },
            "method": {
                "type": "string",
                "default": "post"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "data"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "email": {
                                "type": "string",
                                "default": "yogat@iseka.services"
                            },
                            "password": {
                                "type": "string",
                                "default": "Bali2020%"
                            },
                            "apiUrl": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {
                            "Content-Type": {
                                "type": "string",
                                "default": "text/plain"
                            }
                        }
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "requestBody": {
                                        "type": "string"
                                    },
                                    "requestParams": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "get_all_store_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/43078731-6490-49d3-9980-3edcc72985b5/exec"
            },
            "method": {
                "type": "string",
                "default": "get"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {}
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "header_api_token": {
                                "type": "string"
                            },
                            "region": {
                                "type": "string"
                            },
                            "api_url": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "stores": {
                                        "type": "array",
                                        "items": [{
                                            "type": "object",
                                            "properties": {
                                                "operation_hours": {
                                                    "type": "string",
                                                    "default": "Monday - Sunday (7 AM - 10 PM)"
                                                },
                                                "store_label": {
                                                    "type": "null"
                                                },
                                                "id": {
                                                    "type": "number",
                                                    "default": 13
                                                },
                                                "lat": {
                                                    "type": "string",
                                                    "default": "-6.116145"
                                                },
                                                "store_status": {
                                                    "type": "string",
                                                    "default": "ACTIVE"
                                                },
                                                "created_at": {
                                                    "type": "string",
                                                    "default": "2020-07-27 08:45:29"
                                                },
                                                "region": {
                                                    "type": "string",
                                                    "default": "Jakarta"
                                                },
                                                "store_name": {
                                                    "type": "string",
                                                    "default": "Jakarta PIK"
                                                },
                                                "quinos_name": {
                                                    "type": "string",
                                                    "default": "PIK"
                                                },
                                                "default_region": {
                                                    "type": "number",
                                                    "default": 0
                                                },
                                                "images": {
                                                    "type": "null"
                                                },
                                                "lng": {
                                                    "type": "string",
                                                    "default": "106.757417"
                                                },
                                                "address": {
                                                    "type": "string",
                                                    "default": "Ruko Gallery 8 No. EK-EL, Jl. Pantai Indah Utara 2, RW.7, Kapuk Muara, Penjaringan, North Jakarta City, Jakarta 14460"
                                                },
                                                "phone_number": {
                                                    "type": "string",
                                                    "default": "+628115723568"
                                                },
                                                "logix1_store_id": {
                                                    "type": "string",
                                                    "default": "3690444000001848074"
                                                },
                                                "updated_at": {
                                                    "type": "string",
                                                    "default": "2020-07-31 04:12:01"
                                                }
                                            }
                                        }]
                                    },
                                    "status": {
                                        "type": "number",
                                        "default": 200
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "get_users_detail": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/ba3ae87f-12b0-4d8b-ab74-a7ebcac8685e/exec"
            },
            "method": {
                "type": "string",
                "default": "get"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {}
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "api_url": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "header_api_token": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "id": {
                                        "type": "number",
                                        "default": 1
                                    },
                                    "customer_code": {
                                        "type": "string",
                                        "default": "082110091009"
                                    },
                                    "email": {
                                        "type": "string",
                                        "default": "yogat@iseka.services"
                                    },
                                    "phone_number": {
                                        "type": "string",
                                        "default": "6282237969647"
                                    },
                                    "updated_at": {
                                        "type": "string",
                                        "default": "2020-07-16 03:35:39"
                                    },
                                    "first_name": {
                                        "type": "string",
                                        "default": "Yoga"
                                    },
                                    "remember_token": {
                                        "type": "null"
                                    },
                                    "password": {
                                        "type": "string",
                                        "default": "$2y$10$aXmU5W6v5zWBfaGeDqI/7Oa0Phxfys0U2d0ZZW.bJ3qsRQpopqp.y"
                                    },
                                    "api_token": {
                                        "type": "string",
                                        "default": "ZBPmh6r50jkKI4xQwcWRjBpOEeeRjMedc4lCzNDMZwVG4lCerTEmpckk3ogH"
                                    },
                                    "sur_name": {
                                        "type": "string",
                                        "default": "Tanaya"
                                    },
                                    "date_of_birth": {
                                        "type": "string",
                                        "default": "1995-10-19"
                                    },
                                    "email_verified_at": {
                                        "type": "null"
                                    },
                                    "created_at": {
                                        "type": "string",
                                        "default": "2020-06-23 03:02:02"
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "register_service_already": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/f3055062-d49a-4b74-be20-84321a26c5a4/exec"
            },
            "method": {
                "type": "string",
                "default": "post"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "data"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "phone_number": {
                                "type": "string",
                                "default": "087861588"
                            },
                            "sur_name": {
                                "type": "string",
                                "default": "bagiarta"
                            },
                            "first_name": {
                                "type": "string",
                                "default": "ebuh"
                            },
                            "customer_code": {
                                "type": "string",
                                "default": "087861588"
                            },
                            "apiUrl": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "password": {
                                "type": "string",
                                "default": "Bali2020%"
                            },
                            "email": {
                                "type": "string",
                                "default": "ebuhb@yahoo.com"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {
                            "Content-Type": {
                                "type": "string",
                                "default": "text/plain"
                            }
                        }
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "requestParams": {
                                        "type": "string"
                                    },
                                    "requestBody": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "forgot_password_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/06b1b92b-f823-437c-b3aa-58dc4547eed2/exec"
            },
            "method": {
                "type": "string",
                "default": "post"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "data"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "apiUrl": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "email": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {
                            "Content-Type": {
                                "type": "string",
                                "default": "text/plain"
                            }
                        }
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "message": {
                                        "type": "string",
                                        "default": "We have emailed your password reset link!"
                                    },
                                    "data": {
                                        "type": "array",
                                        "items": [{
                                            "type": "null"
                                        }]
                                    },
                                    "status": {
                                        "type": "number",
                                        "default": 200
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "register_service_new": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/53da4780-7ad6-4db4-8fd4-487296cd55e6/exec"
            },
            "method": {
                "type": "string",
                "default": "post"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "data": {
                                "type": "data"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "apiUrl": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "sur_name": {
                                "type": "string"
                            },
                            "password": {
                                "type": "string"
                            },
                            "first_name": {
                                "type": "string"
                            },
                            "phone_number": {
                                "type": "string"
                            },
                            "email": {
                                "type": "string"
                            },
                            "customer_code": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {
                            "Content-Type": {
                                "type": "string",
                                "default": "text/plain"
                            }
                        }
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "Message": {
                                        "type": "string",
                                        "default": "Registration Succesfull"
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "get_store_details_service": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/078a94a8-633b-4279-b40d-dd45c5fa1193/exec"
            },
            "method": {
                "type": "string",
                "default": "get"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {}
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "api_url": {
                                "type": "string",
                                "default": "{Settings.apiUrl}"
                            },
                            "header_api_token": {
                                "type": "string"
                            },
                            "id": {
                                "type": "string"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "status": {
                                        "type": "number",
                                        "default": 200
                                    },
                                    "store": {
                                        "type": "object",
                                        "properties": {
                                            "address": {
                                                "type": "null"
                                            },
                                            "region": {
                                                "type": "string",
                                                "default": "Surabaya"
                                            },
                                            "phone_number": {
                                                "type": "null"
                                            },
                                            "store_label": {
                                                "type": "null"
                                            },
                                            "lng": {
                                                "type": "null"
                                            },
                                            "store_name": {
                                                "type": "string",
                                                "default": "Surabaya Rungkut"
                                            },
                                            "id": {
                                                "type": "number",
                                                "default": 2
                                            },
                                            "operation_hours": {
                                                "type": "string",
                                                "default": "Brewing Soon"
                                            },
                                            "logix1_store_id": {
                                                "type": "string",
                                                "default": "3690444000003631020"
                                            },
                                            "default_region": {
                                                "type": "number",
                                                "default": 0
                                            },
                                            "quinos_name": {
                                                "type": "string",
                                                "default": "TANAMERA-RGNKT"
                                            },
                                            "images": {
                                                "type": "null"
                                            },
                                            "lat": {
                                                "type": "null"
                                            },
                                            "store_status": {
                                                "type": "string",
                                                "default": "ACTIVE"
                                            },
                                            "created_at": {
                                                "type": "string",
                                                "default": "2020-07-27T08:45:29.000000Z"
                                            },
                                            "updated_at": {
                                                "type": "string",
                                                "default": "2020-07-31T04:12:01.000000Z"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    },
    "get_store_regions": {
        "type": "object",
        "properties": {
            "url": {
                "type": "string",
                "default": "https://api.appery.io/rest/1/code/26b62a75-a9ea-46b1-a1fd-e599b929c9b7/exec"
            },
            "method": {
                "type": "string",
                "default": "get"
            },
            "request": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {}
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "header_api_token": {
                                "type": "string",
                                "default": "Bearer bmWpHVhmRZigXRjBtrDJmcbvIxkaqCTV860HR7O9XKNMjW4ua64YkAG6f62I"
                            },
                            "api_url": {
                                "type": "string",
                                "default": "https://147.139.134.25/mytanamera/api/"
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            },
            "response": {
                "type": "object",
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": {
                            "$": {
                                "type": "object",
                                "properties": {
                                    "regions": {
                                        "type": "array",
                                        "items": [{
                                            "type": "object",
                                            "properties": {
                                                "region": {
                                                    "type": "string",
                                                    "default": "Bali"
                                                }
                                            }
                                        }]
                                    },
                                    "status": {
                                        "type": "number",
                                        "default": 200
                                    }
                                }
                            }
                        }
                    },
                    "headers": {
                        "type": "object",
                        "properties": {}
                    }
                }
            }
        }
    }
};
/**
 * Data storage
 */