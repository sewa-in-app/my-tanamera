import {
    ExportedClass as login_api_service
} from './services/login_api_service';
import {
    ExportedClass as get_users_detail
} from './services/get_users_detail';
import {
    ExportedClass as update_user_service
} from './services/update_user_service';
import {
    ExportedClass as get_balance_service
} from './services/get_balance_service';
import {
    ExportedClass as register_service_new
} from './services/register_service_new';
import {
    ExportedClass as register_service_already
} from './services/register_service_already';
import {
    ExportedClass as forgot_password_service
} from './services/forgot_password_service';
import {
    ExportedClass as get_store_regions
} from './services/get_store_regions';
import {
    ExportedClass as get_all_store_service
} from './services/get_all_store_service';
import {
    ExportedClass as get_store_details_service
} from './services/get_store_details_service';
import {
    ExportedClass as change_password_service
} from './services/change_password_service';
import {
    ExportedClass as AuthInterceptor
} from './custom/AuthInterceptor';
import {
    ExportedClass as CountriesPhoneCode
} from './custom/CountriesPhoneCode';
export const services = {
    login_api_service,
    get_users_detail,
    update_user_service,
    get_balance_service,
    register_service_new,
    register_service_already,
    forgot_password_service,
    get_store_regions,
    get_all_store_service,
    get_store_details_service,
    change_password_service,
    AuthInterceptor,
    CountriesPhoneCode,
};