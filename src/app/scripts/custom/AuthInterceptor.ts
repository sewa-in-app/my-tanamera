import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import 'rxjs/add/operator/do';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
  HttpInterceptor
} from '@angular/common/http';
import { ApperyioHelperService } from '../apperyio/apperyio_helper';
/*
  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
class AuthInterceptor implements HttpInterceptor {
    constructor(private Apperyio: ApperyioHelperService) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        var token = this.Apperyio.data.getStorage("api_token_only");
        console.log(token);
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${token}`
            }
        });
        return next.handle(request).do((event: HttpEvent<any>) => {
            if(event instanceof HttpResponse){
                console.log(event);
            }
        }, (err: any) => {
            if(err instanceof HttpErrorResponse){
                if(err.status === 401){
                    this.Apperyio.navigateTo("Login");
                }
            }
        })
    }
}

/*
    Service class should be exported as ExportedClass
*/
export { AuthInterceptor as ExportedClass };