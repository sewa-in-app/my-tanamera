// Appery.io models
export interface $aio_empty_object {};
//
export interface update_user_serviceResponse {
    "Message": string
}
//
interface __get_balance_serviceResponse_sub_006 extends Array < null > {}
interface __get_balance_serviceResponse_sub_005 extends Array < null > {}
interface __get_balance_serviceResponse_sub_004 extends Array < null > {}
interface __get_balance_serviceResponse_sub_003 extends Array < null > {}
interface __get_balance_serviceResponse_sub_002 {
    "Discount4": __get_balance_serviceResponse_sub_003,
    "discount2_id": null,
    "Discount3": __get_balance_serviceResponse_sub_004,
    "name": string,
    "discount4_id": null,
    "discount3_id": null,
    "Discount2": __get_balance_serviceResponse_sub_005,
    "Discount1": __get_balance_serviceResponse_sub_006,
    "discount1_id": null
}
interface __get_balance_serviceResponse_sub_001 {
    "city": string,
    "name": string,
    "membership_id": string,
    "total_spent": string,
    "notes": string,
    "joined": null,
    "occupation": string,
    "state": string,
    "total_visit": string,
    "mobile": string,
    "address": string,
    "balance": string,
    "postcode": string,
    "company_id": string,
    "expired": null,
    "phone": string,
    "dob": string,
    "email": string,
    "modified": string,
    "oid": string,
    "created": string,
    "gender": string,
    "id": string,
    "code": string,
    "active": string,
    "last_visit": string
}
export interface get_balance_serviceResponse {
    "Customer": __get_balance_serviceResponse_sub_001,
    "Membership": __get_balance_serviceResponse_sub_002
}
//
export interface change_password_serviceResponse {
    "message": string,
    "status": number
}
//
export interface login_api_serviceResponse {
    "requestBody": string,
    "requestParams": string
}
//
interface __get_all_store_serviceResponse_sub_002 {
    "operation_hours": string,
    "store_label": null,
    "id": number,
    "lat": string,
    "store_status": string,
    "created_at": string,
    "region": string,
    "store_name": string,
    "quinos_name": string,
    "default_region": number,
    "images": null,
    "lng": string,
    "address": string,
    "phone_number": string,
    "logix1_store_id": string,
    "updated_at": string
}
interface __get_all_store_serviceResponse_sub_001 extends Array < __get_all_store_serviceResponse_sub_002 > {}
export interface get_all_store_serviceResponse {
    "stores": __get_all_store_serviceResponse_sub_001,
    "status": number
}
//
export interface get_users_detailResponse {
    "id": number,
    "customer_code": string,
    "email": string,
    "phone_number": string,
    "updated_at": string,
    "first_name": string,
    "remember_token": null,
    "password": string,
    "api_token": string,
    "sur_name": string,
    "date_of_birth": string,
    "email_verified_at": null,
    "created_at": string
}
//
export interface register_service_alreadyResponse {
    "requestParams": string,
    "requestBody": string
}
//
interface __forgot_password_serviceResponse_sub_001 extends Array < null > {}
export interface forgot_password_serviceResponse {
    "message": string,
    "data": __forgot_password_serviceResponse_sub_001,
    "status": number
}
//
export interface register_service_newResponse {
    "Message": string
}
//
interface __get_store_details_serviceResponse_sub_001 {
    "address": null,
    "region": string,
    "phone_number": null,
    "store_label": null,
    "lng": null,
    "store_name": string,
    "id": number,
    "operation_hours": string,
    "logix1_store_id": string,
    "default_region": number,
    "quinos_name": string,
    "images": null,
    "lat": null,
    "store_status": string,
    "created_at": string,
    "updated_at": string
}
export interface get_store_details_serviceResponse {
    "status": number,
    "store": __get_store_details_serviceResponse_sub_001
}
//
interface __get_store_regionsResponse_sub_002 {
    "region": string
}
interface __get_store_regionsResponse_sub_001 extends Array < __get_store_regionsResponse_sub_002 > {}
export interface get_store_regionsResponse {
    "regions": __get_store_regionsResponse_sub_001,
    "status": number
}