export const constants = {
    /**
     * Settings
     * @property logix1_token       - Auth Token of Logix1 - Coffee House
     * @property quinos_token       - 
     * @property apiUrl       - 
     * @property quinos_api_url       - 
     */
    Settings: {
        "logix1_token": "10010790796.10019059024.f1b8ae4f6bdba340b6a328244fe214dcd3a6c2e53fbdeb52283cadd496d787b93d6c974b60829e9b06510bcc38c7c2784f0f1e65dfca15ecb7202cbdde1c6510",
        "quinos_token": "44dc4fe08074d32657b61256043da67f2b414f6d",
        "apiUrl": "https://mytanamera.logix1.com/mytanamera/api/",
        "quinos_api_url": "http://quinoscloud.com/cloud/customers/getCustomerByCode/"
    }
};
export const routes = {
    "Home": "home",
    "Profile": "profile",
    "SplashScreen": "splashscreen",
    "Login": "login",
    "YourPoint": "yourpoint",
    "Tabs": "tabs",
    "Profile_Tabs_Plugin": "profile",
    "NiceTabs": "nicetabs",
    "SplashScreen2": "splashscreen2",
    "Edit_Info": "edit_info",
    "Register": "register",
    "Autocomplete": "autocomplete",
    "Register_Member_Already": "register_member_already",
    "Forgot_Password": "forgot_password",
    "Outlets": "outlets",
    "Stores": "stores/:region",
    "Store_Detail": "store_detail/:id",
    "Settings": "settings",
    "Change_Password": "change_password",
    "Notification_Settings": "notification_settings",
    "Points_History": "points_history",
    "Settings_clone_1": "settings_clone_1",
    "Profile_clone_1": "profile_clone_1",
};
export const pushSettings = {
    appID: 'e07dbc7d-1eb1-4d72-a72a-d777d8be52d0',
    baseUrl: 'https://api.appery.io/rest/push/reg',
    initOptions: {}
};