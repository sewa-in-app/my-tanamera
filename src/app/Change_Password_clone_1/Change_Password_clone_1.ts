import {
    Component,
    ChangeDetectorRef
} from '@angular/core';
import {
    ApperyioHelperService
} from '../scripts/apperyio/apperyio_helper';
import {
    ApperyioMappingHelperService
} from '../scripts/apperyio/apperyio_mapping_helper';
import {
    $aio_empty_object
} from '../scripts/interfaces';
import {
    ViewChild
} from '@angular/core';
@Component({
    templateUrl: 'Change_Password_clone_1.html',
    selector: 'page-change_-password_clone_1',
    styleUrls: ['Change_Password_clone_1.scss']
})
export class Change_Password_clone_1 {
    public current_password: string;
    public confirm_password: string;
    public new_password: string;
    public message: string;
    public currentItem: any = null;
    public mappingData: any = {};
    public __getMapping(_currentItem, property, defaultValue, isVariable ? , isSelected ? ) {
        return this.$aio_mappingHelper.getMapping(this.mappingData, _currentItem, property, defaultValue, isVariable, isSelected);
    }
    constructor(public Apperyio: ApperyioHelperService, private $aio_mappingHelper: ApperyioMappingHelperService, private $aio_changeDetector: ChangeDetectorRef) {}
    async btnsavenewpasswordClick__j_51(event ? , currentItem ? ) {
        /* Invoke data service */
        this.invokeService_change_password_clone_1();
    }
    private $aio_dataServices = {
        "change_password_clone_1": "invokeService_change_password_clone_1"
    }
    invokeService_change_password_clone_1(cb ? : Function) {
        this.Apperyio.getService("change_password_service").then(
            async (service) => {
                if (!service) {
                    console.log("Error. Service was not found.");
                    return;
                }
                let data = {}
                let params = {}
                let headers = {}
                this.$aio_changeDetector.detectChanges();
                /* Mapping */
                params = this.$aio_mappingHelper.updateData(params, ['confirm_password'], this.$aio_mappingHelper.getSubdata(this.confirm_password, []));
                params = this.$aio_mappingHelper.updateData(params, ['new_password'], this.$aio_mappingHelper.getSubdata(this.new_password, []));
                params = this.$aio_mappingHelper.updateData(params, ['current_password'], this.$aio_mappingHelper.getSubdata(this.current_password, []));
                params = this.$aio_mappingHelper.updateData(params, ['header_api_token'], await this.$aio_mappingHelper.getStorageValue("header_api_token", []));
                /* Present Loading */
                await (async () => {
                    let options = {}
                    let controller = this.Apperyio.getController('LoadingController');
                    const loading = await controller.create(options);
                    return await loading.present();
                })();
                service.execute({
                    data: data,
                    params: params,
                    headers: headers
                }).subscribe(
                    /* onsuccess */
                    async (res: any) => {
                        let mappingData: any = {};
                        /* Mapping */
                        this.message = this.$aio_mappingHelper.updateData(this.message, [], this.$aio_mappingHelper.getSubdata(res, ["message"]));
                        /* Run TypeScript */
                        var message = this.message;
                        this.Apperyio.getController("ToastController").create({
                            header: 'Response:',
                            message: message,
                            position: 'top',
                            buttons: [{
                                side: 'start',
                                icon: 'checkmark',
                                handler: () => {
                                    console.log('Favorite clicked');
                                }
                            }, {
                                text: 'Done',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]
                        }).then(toast => toast.present());
                        /* Dismiss loading */
                        await this.Apperyio.getController("LoadingController").dismiss();
                        this.mappingData = { ...this.mappingData,
                            ...mappingData
                        };
                        if (cb && typeof cb === "function") cb(res);
                    },
                    (err: any) => {
                        console.log(err);
                    }
                )
            }
        );
    }
}